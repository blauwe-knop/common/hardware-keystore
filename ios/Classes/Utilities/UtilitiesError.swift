import Foundation

public enum UtilitiesError: Error {
    case PlatformError(reason: String)
    case BridgingError(reason: String)

}
