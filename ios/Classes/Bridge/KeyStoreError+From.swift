//
//  KeyStoreError+From.swift
//  HWKeyStore
//
//  Created by Wallet Developer on 27/02/2023.
//

import Foundation

public enum KeyStoreError: Error {
    case KeyError(reason: String)
    case BridgingError(reason: String)
    case RandomError(reason: String) //TODO: Create proper error handling
}

extension KeyStoreError {
    static func from(_ error: SecureEnclaveKeyError) -> Self {
        return .KeyError(reason: error.localizedDescription)
    }
}
