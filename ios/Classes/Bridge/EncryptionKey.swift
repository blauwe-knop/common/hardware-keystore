import Foundation

final class EncryptionService {
    private static let identifierPrefix = "ecies"

    private func secureEnclaveKey(for identifier: String) throws -> SecureEnclaveKey {
        return try SecureEnclaveKey(identifier:  "\(Self.identifierPrefix)_\(identifier)")
    }
}
extension EncryptionService {
    func encrypt(identifier: String, plainText: [UInt8]) throws -> Data {
        do {
            return try self.secureEnclaveKey(for: identifier).encrypt(plainText: Data(plainText))
        } catch let error as SecureEnclaveKeyError {
            throw KeyStoreError.from(error)
        }
    }

    func decrypt(identifier: String, cipherText: Data) throws -> Data {
        do {
            return try self.secureEnclaveKey(for: identifier).decrypt(cipherText: cipherText)
        } catch let error as SecureEnclaveKeyError {
            throw KeyStoreError.from(error)
        }
    }

    func delete(identifier: String) throws {
        do {
            return try self.secureEnclaveKey(for: identifier).delete()
        } catch let error as SecureEnclaveKeyError {
            throw KeyStoreError.from(error)
        }
    }
    
    func createKeyPair(identifier: String) throws {
        do {
            try SecureEnclaveKey.createKey(with: "\(Self.identifierPrefix)_\(identifier)")
        } catch let error as SecureEnclaveKeyError {
            throw KeyStoreError.from(error)
        } catch let error {
            throw KeyStoreError.RandomError(reason: "Random error: \(error)")
        }
    }
    
}
