//
//  SigningKey.swift
//  HWKeyStore
//
//  Created by Wallet Developer on 24/02/2023.
//

import Foundation

public protocol SigningKeyBridge : AnyObject {
    func `publicKey`(`identifier`: String) throws -> String
    func `sign`(`identifier`: String, `payload`: [UInt8]) throws -> Data
    
}

final class SigningService {
    private static let identifierPrefix = "ecdsa"
    
    private func secureEnclaveKey(for identifier: String) throws -> SecureEnclaveKey {
        return try SecureEnclaveKey(identifier: "\(Self.identifierPrefix)_\(identifier)")
    }
}

extension SigningService: SigningKeyBridge {
    func publicKey(identifier: String) throws -> String {
        do {
            return try self.secureEnclaveKey(for: identifier).encodePublicKey()
        } catch let error as SecureEnclaveKeyError {
            throw KeyStoreError.from(error)
        }
    }
    
    func sign(identifier: String, payload: [UInt8]) throws -> Data {
        do {
            return try self.secureEnclaveKey(for: identifier).sign(payload: Data(payload))
        } catch let error as SecureEnclaveKeyError {
            throw KeyStoreError.from(error)
        }
    }

   func verify(identifier: String, payload: [UInt8], signature: Data) throws -> Bool {
        return try self.secureEnclaveKey(for: identifier).verify(payload: Data(payload), signature: signature)
   }
    
    func createKeyPair(identifier: String) throws {
        do {
            try SecureEnclaveKey.createKey(with: "\(Self.identifierPrefix)_\(identifier)")
        } catch let error as SecureEnclaveKeyError {
            throw KeyStoreError.from(error)
        } catch let error {
            throw KeyStoreError.RandomError(reason: "Random error: \(error)")
        }
    }

    func delete(identifier: String) throws {
        do {
            return try self.secureEnclaveKey(for: identifier).delete()
        } catch let error as SecureEnclaveKeyError {
            throw KeyStoreError.from(error)
        }
    }
}
