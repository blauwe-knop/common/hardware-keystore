import Flutter
import UIKit

enum ExposedMethods: String {
    case getPlatformVersion = "getPlatformVersion"
    case getPublicKey = "getPublicKey"
    case deleteKeyPairs = "deleteKeyPairs"
    case generateKeyPairs = "generateKeyPairs"
    case sign = "sign"
    case verify = "verify"
    case encrypt = "encrypt"
    case decrypt = "decrypt"
    case generateSingingKey = "generateSigningKey"
    case deleteSigningKey = "deleteSigningKey"
    case generateEncryptionKey = "generateEncryptionKey"
    case deleteEncryptionKey = "deleteEncryptionKey"
}

public class HardwareKeystorePlugin: NSObject, FlutterPlugin {
    
    private var platformSupport = PlatformSupport.shared
    
    public static func register(with registrar: FlutterPluginRegistrar) {
        let channel = FlutterMethodChannel(name: "hardware_keystore", binaryMessenger: registrar.messenger())
        let instance = HardwareKeystorePlugin()
        registrar.addMethodCallDelegate(instance, channel: channel)
    }
    
    public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
        switch call.method {
        case ExposedMethods.getPlatformVersion.rawValue:
            result("iOS " + UIDevice.current.systemVersion)
        case ExposedMethods.getPublicKey.rawValue:
            getPublicKey(call, result: result)
        case ExposedMethods.sign.rawValue:
            sign(call, result: result)
        case ExposedMethods.generateKeyPairs.rawValue:
            generateKeyPairs(call, result: result)
        case ExposedMethods.verify.rawValue:
            verify(call, result: result)
        case ExposedMethods.deleteKeyPairs.rawValue:
            deleteKeyPair(call, result: result)
        case ExposedMethods.encrypt.rawValue:
            encrypt(call, result: result)
        case ExposedMethods.decrypt.rawValue:
            decrypt(call, result: result)
        case ExposedMethods.generateSingingKey.rawValue:
            generateSigningKey(call, result: result)
        case ExposedMethods.deleteSigningKey.rawValue:
            deleteSingingKey(call, result: result)
        case ExposedMethods.generateEncryptionKey.rawValue:
            generateEncryptionKey(call, result: result)
        case ExposedMethods.deleteEncryptionKey.rawValue:
            deleteEncryptionKey(call, result: result)
        default:
            result(FlutterMethodNotImplemented)
        }
    }
    
    private func getPublicKey(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
        do {
            guard let args = call.arguments as? Dictionary<String, Any> else {
                result(KeyStoreError.RandomError(reason: "No variables were provided").localizedDescription)
                return
            }
            guard let id = args["identifier"] as? String else {
                result(KeyStoreError.RandomError(reason: "No identifier was provided").localizedDescription)
                return
            }
            let signingService = platformSupport.signingService
            let data = try signingService.publicKey(identifier: id)
            result(data)
        }
        catch {
            result(KeyStoreError.RandomError(reason: "\(error)").localizedDescription)
        }
    }
    
    private func sign(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
        do {
            guard let args = call.arguments as? Dictionary<String, Any> else {
                result(KeyStoreError.RandomError(reason: "No variables were provided").localizedDescription)
                return
            }
            guard let id = args["identifier"] as? String else {
                result(KeyStoreError.RandomError(reason: "No identifier was provided").localizedDescription)
                return
            }
            guard let message = args["message"] as? String else {
                result(KeyStoreError.RandomError(reason: "No message was provided").localizedDescription)
                return
            }
            
            let response: Data = try platformSupport.signingService.sign(identifier: id, payload: Array(message.utf8))
            let responseAsString: String = response.base64EncodedString()
            result(responseAsString)
        }
        catch {
            result(KeyStoreError.RandomError(reason: "\(error)").localizedDescription)
        }
    }
    
    private func verify(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
        guard let args = call.arguments as? Dictionary<String, Any> else {
            result(KeyStoreError.RandomError(reason: "No variables were provided").localizedDescription)
            return
        }
        guard let id = args["identifier"] as? String else {
            result(KeyStoreError.RandomError(reason: "No identifier was provided").localizedDescription)
            return
        }
        guard let message = args["message"] as? String else {
            result(KeyStoreError.RandomError(reason: "No message was provided").localizedDescription)
            return
        }
        guard let signatureBase64 = args["signature"]  as? String else {
            result(KeyStoreError.RandomError(reason: "No signature was provided").localizedDescription)
            return
        }
        guard let signatureData =  Data(base64Encoded: signatureBase64) as? Data else {
            result(KeyStoreError.RandomError(reason: "").localizedDescription)
            return
        }
        do {
            try result(platformSupport.signingService.verify(identifier: id, payload: Array(message.utf8), signature: signatureData))
        } catch {
            result(KeyStoreError.RandomError(reason: "\(error)").localizedDescription)
            
        }
    }
    
    private func generateKeyPairs(_ call: FlutterMethodCall, result: @escaping FlutterResult ) {
        do {
            guard let args = call.arguments as? Dictionary<String, Any> else {
                result(KeyStoreError.RandomError(reason: "No variables were provided").localizedDescription)
                return
            }
            guard let id = args["identifier"] as? String else {
                result(KeyStoreError.RandomError(reason: "No identifier was provided").localizedDescription)
                return
            }
            
            try platformSupport.signingService.createKeyPair(identifier: id)
            result(true)
        }
        catch {
            result(KeyStoreError.RandomError(reason: "\(error)").localizedDescription)
        }
    }
    
    private func deleteKeyPair(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
        guard let args = call.arguments as? Dictionary<String, Any> else {
            result(KeyStoreError.RandomError(reason: "No variables were provided").localizedDescription)
            return
        }
        guard let id = args["identifier"] as? String else {
            result(KeyStoreError.RandomError(reason: "No identifier was provided").localizedDescription)
            return
        }
        do {
            try platformSupport.signingService.delete(identifier: id)
            result(true)
        } catch {
            result(KeyStoreError.RandomError(reason: "\(error)").localizedDescription)
        }
    }
    
    private func encrypt(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
        do {
            guard let args = call.arguments as? Dictionary<String, Any> else {
                result(KeyStoreError.RandomError(reason: "No variables were provided").localizedDescription)
                return
            }
            guard let id = args["identifier"] as? String else {
                result(KeyStoreError.RandomError(reason: "No identifier was provided").localizedDescription)
                return
            }
            guard let plainText = args["payload"] as? String else {
                result(KeyStoreError.RandomError(reason: "No plaintext was provided").localizedDescription)
                return
            }
            let response: Data = try platformSupport.encryptionService.encrypt(identifier: id, plainText: Array(plainText.utf8))
            let responseAsString: String = response.base64EncodedString()
            result(responseAsString)
        } catch {
            result(KeyStoreError.RandomError(reason: "\(error)").localizedDescription)
        }
    }
    
    private func decrypt(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
        do {
            guard let args = call.arguments as? Dictionary<String, Any> else {
                result(KeyStoreError.RandomError(reason: "No variables were provided").localizedDescription)
                return
            }
            guard let id = args["identifier"] as? String else {
                result(KeyStoreError.RandomError(reason: "No identifier was provided").localizedDescription)
                return
            }
            guard let cipherTextBase64 = args["cipher"] as? String else {
                result(KeyStoreError.RandomError(reason: "No ciphertext was provided").localizedDescription)
                return
            }
            guard let cipherText =  Data(base64Encoded: cipherTextBase64) as? Data else {
                result(KeyStoreError.RandomError(reason: "").localizedDescription)
                return
            }
            let clearText: Data = try platformSupport.encryptionService.decrypt(identifier: id, cipherText: cipherText)
            let plainText = String.init(data: clearText, encoding: .utf8)!
            result(plainText)
        } catch {
            result(KeyStoreError.RandomError(reason: "\(error)").localizedDescription)
        }
    }
    
    private func generateSigningKey(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
        do {
            guard let args = call.arguments as? Dictionary<String, Any> else {
                result(KeyStoreError.RandomError(reason: "No variables were provided").localizedDescription)
                return
            }
            guard let id = args["identifier"] as? String else {
                result(KeyStoreError.RandomError(reason: "No identifier was provided").localizedDescription)
                return
            }
            
            try platformSupport.signingService.createKeyPair(identifier: id)
            result(true)
        }
        catch {
            result(KeyStoreError.RandomError(reason: "\(error)").localizedDescription)
        }
    }
    
    private func deleteSingingKey(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
        guard let args = call.arguments as? Dictionary<String, Any> else {
            result(KeyStoreError.RandomError(reason: "No variables were provided").localizedDescription)
            return
        }
        guard let id = args["identifier"] as? String else {
            result(KeyStoreError.RandomError(reason: "No identifier was provided").localizedDescription)
            return
        }
        do {
            try platformSupport.signingService.delete(identifier: id)
            result(true)
        } catch {
            result(KeyStoreError.RandomError(reason: "\(error)"))
        }
    }
    
    private func generateEncryptionKey(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
        do {
            guard let args = call.arguments as? Dictionary<String, Any> else {
                result(KeyStoreError.RandomError(reason: "No variables were provided").localizedDescription)
                return
            }
            guard let id = args["identifier"] as? String else {
                result(KeyStoreError.RandomError(reason: "No identifier was provided").localizedDescription)
                return
            }
            
            try platformSupport.encryptionService.createKeyPair(identifier: id)
            result(true)
        }
        catch {
            result(KeyStoreError.RandomError(reason: "\(error)").localizedDescription)
        }
    }
    
    private func deleteEncryptionKey(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
        guard let args = call.arguments as? Dictionary<String, Any> else {
            result(KeyStoreError.RandomError(reason: "No variables were provided").localizedDescription)
            return
        }
        guard let id = args["identifier"] as? String else {
            result(KeyStoreError.RandomError(reason: "No identifier was provided").localizedDescription)
            return
        }
        do {
            try platformSupport.encryptionService.delete(identifier: id)
            result(true)
        } catch {
            result(KeyStoreError.RandomError(reason: "\(error)").localizedDescription)
        }
        
    }
}
