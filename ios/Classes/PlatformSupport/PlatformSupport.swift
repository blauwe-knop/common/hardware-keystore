//
//  PlatformSupport.swift
//  PlatformSupport
//
//  Created by The Wallet Developers on 13/04/2023.
//

import Foundation

public final class PlatformSupport {
    public static let shared = PlatformSupport()

    let signingService: SigningService
    let encryptionService: EncryptionService
    let utilities: Utilities

    private init() {
        self.signingService = SigningService()
        self.utilities = Utilities()
        self.encryptionService = EncryptionService()
    }
}
