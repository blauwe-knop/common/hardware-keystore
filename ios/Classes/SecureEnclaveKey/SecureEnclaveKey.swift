import Foundation
import Security
import CryptoKit

final class SecureEnclaveKey {

    private static let pemHeader :[UInt8] = [
        0x30, 0x81, 0x9f, 0x30, 0x0d, 0x06, 0x09, 0x2a,
        0x86, 0x48, 0x86, 0xf7, 0x0d, 0x01, 0x01, 0x01,
        0x05, 0x00, 0x03, 0x81, 0x8d, 0x00
    ]

    private static let signingAlgorithm: SecKeyAlgorithm = .ecdsaSignatureMessageX962SHA256
    private static let encryptionAlgorithm: SecKeyAlgorithm = .eciesEncryptionCofactorVariableIVX963SHA256AESGCM

    private static let queue = DispatchQueue(label: String(describing: SecureEnclaveKey.self), qos: .userInitiated)


    private static func tag(from identifier: String) -> Data {
        return identifier.data(using: .utf8)!
    }

    private static func error(for unmanagedError: Unmanaged<CFError>?) -> Error? {
        guard let unmanagedError else {
            return nil
        }

        let error = unmanagedError.takeRetainedValue()

        return error
    }

    private static func errorMessage(for status: OSStatus) -> String? {
        guard #available(iOS 11.3, *),
              let errorMessage = SecCopyErrorMessageString(status, nil) else {
            return nil
        }
        return errorMessage as String
    }

    private static func fetchKey(with identifier: String) throws -> SecKey? {
        let query: [String: Any] = [
            kSecClass as String: kSecClassKey,
            kSecAttrTokenID as String: kSecAttrTokenIDSecureEnclave,
            kSecAttrApplicationTag as String: self.tag(from: identifier),
            kSecAttrKeyType as String: kSecAttrKeyTypeEC,
            kSecReturnRef as String: true
        ]

        var item: CFTypeRef?
        let status = SecItemCopyMatching(query as CFDictionary, &item)

        switch status {
        case errSecSuccess:
            print("key found")
            break
        case errSecItemNotFound:
            throw SecureEnclaveKeyError.noKey(errorMessage: "no key found")
        default:
            let errorMessage: String? = {
                guard #available(iOS 11.3, *),
                      let errorMessage = SecCopyErrorMessageString(status, nil) else {
                    return nil
                }

                return errorMessage as String
            }()

            throw SecureEnclaveKeyError.fetch(errorMessage: errorMessage)
        }

        // swiftformat:disable redundantParens
        return (item as! SecKey)
        // swiftformat:enable redundantParens
    }

    public static func createKey(with identifier: String) throws -> SecKey {
        var error: Unmanaged<CFError>?

        guard let access = SecAccessControlCreateWithFlags(
            kCFAllocatorDefault,
            kSecAttrAccessibleWhenUnlockedThisDeviceOnly,
            .privateKeyUsage,
            &error
        ) else {
            throw SecureEnclaveKeyError.create(keyChainError: self.error(for: error))
        }

        let keyAttributes: [String: Any] = [
            kSecAttrIsPermanent as String: true,
            kSecAttrApplicationTag as String: self.tag(from: identifier),
            kSecAttrAccessControl as String: access
        ]
        let attributes: [String: Any] = [
            kSecAttrTokenID as String: kSecAttrTokenIDSecureEnclave,
            kSecAttrKeyType as String: kSecAttrKeyTypeEC,
            kSecAttrKeySizeInBits as String: 256,
            kSecPrivateKeyAttrs as String: keyAttributes
        ]

        guard let key = SecKeyCreateRandomKey(attributes as CFDictionary, &error) else {
            throw SecureEnclaveKeyError.create(keyChainError: self.error(for: error))
        }

        return key
    }

    private static func encode(publicKey: SecKey) throws -> String {
        var error: Unmanaged<CFError>?
        guard let keyData = SecKeyCopyExternalRepresentation(publicKey, &error) else {
            throw SecureEnclaveKeyError.derivePublicKey(keyChainError: self.error(for: error))
        }
        let data: Data = keyData as Data
        let p256PublicKey = try P256.Signing.PublicKey(x963Representation: data)

        return p256PublicKey.pemRepresentation
    }

    // MARK: - Instance properties

    let identifier: String

    private var privateKey: SecKey?

    private var publicKey: SecKey? {
        if privateKey == nil {
           return nil
        }
        guard let publicKey = SecKeyCopyPublicKey(privateKey!) else {
            fatalError("Error while deriving public key")
        }

        return publicKey
    }

    // MARK: - Initializer

    init(identifier: String) throws {
        self.identifier = identifier

        self.privateKey = try Self.queue.sync(execute: {
            let privateKey = try Self.fetchKey(with: identifier)
            return privateKey
        })
    }

    // MARK: - Instance methods

    func encodePublicKey() throws -> String {
        if privateKey == nil {
            throw SecureEnclaveKeyError.noKey(errorMessage: "privatekey nill")
        }
        return try Self.encode(publicKey: self.publicKey!)
    }

    func createKeyPair(identifier: String) throws {
        self.privateKey = try Self.queue.sync( execute: {
            return try Self.createKey(with: identifier)
        })
    }

    func sign(payload: Data) throws -> Data {
        var error: Unmanaged<CFError>?
        if privateKey == nil {
            throw SecureEnclaveKeyError.noKey(errorMessage: "private key nill")
        }
        guard let signature = SecKeyCreateSignature(self.privateKey!,
                                                    SecureEnclaveKey.signingAlgorithm,
                                                    payload as CFData,
                                                    &error) else {
            throw SecureEnclaveKeyError.sign(keyChainError: Self.error(for: error))
        }
        return signature as Data
    }

    func verify(payload: Data, signature: Data) throws -> Bool {
        var error: Unmanaged<CFError>?
        let publicKey = self.publicKey!
        let isValid = SecKeyVerifySignature(publicKey, SecureEnclaveKey.signingAlgorithm, payload as CFData, signature as CFData, &error)

        if error != nil {
            throw error!.takeRetainedValue() as Error
        }

        if let error, CFErrorGetCode(error.takeRetainedValue()) != errSecVerifyFailed {
            preconditionFailure("Could not verify signature: \(error)")
        }
        return isValid
    }


    func encrypt(plainText: Data) throws -> Data {
        var error: Unmanaged<CFError>?
        let publicKey = self.publicKey!
        guard SecKeyIsAlgorithmSupported(publicKey, .encrypt, SecureEnclaveKey.encryptionAlgorithm) else {
            throw SecureEnclaveKeyError.encrypt(errorMessage: "algorithm not supported")
        }
        guard let cipherText = SecKeyCreateEncryptedData(publicKey,
                                SecureEnclaveKey.encryptionAlgorithm,
                                plainText as CFData,
                                &error) as Data? else {
                                    throw SecureEnclaveKeyError.encrypt(errorMessage: "failed to encrypt data")
                                }
        return cipherText as Data
    }

    func decrypt(cipherText: Data) throws -> Data {
       var error: Unmanaged<CFError>?
       let publicKey = self.publicKey!
       if privateKey == nil {
           throw SecureEnclaveKeyError.noKey(errorMessage: "private key nill")
       }
       let privateKey = self.privateKey!
       guard SecKeyIsAlgorithmSupported(publicKey, .encrypt, SecureEnclaveKey.encryptionAlgorithm) else {
           throw SecureEnclaveKeyError.decrypt(errorMessage: "algorithm not supported")
       }
       guard let clearText = SecKeyCreateDecryptedData(privateKey,
                              SecureEnclaveKey.encryptionAlgorithm,
                              cipherText as CFData,
                              &error) as Data? else {
                              throw SecureEnclaveKeyError.decrypt(errorMessage: "failed to decrypt: \(error)")
                              }
       guard clearText != nil else {
           throw SecureEnclaveKeyError.decrypt(errorMessage: "no clear text. decryption failed")
       }
       return clearText as Data
    }

   consuming func delete() throws {
        let query: [String: Any] = [
            kSecClass as String: kSecClassKey,
            kSecAttrIsPermanent as String: true,
            kSecAttrApplicationTag as String: Self.tag(from: self.identifier)
        ]
        let status = SecItemDelete(query as CFDictionary)

        guard status == errSecSuccess else {
            throw SecureEnclaveKeyError.delete(errorMessage: Self.errorMessage(for: status))
        }
    }

}
