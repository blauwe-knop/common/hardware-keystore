import 'package:flutter/material.dart';
import 'dart:async';

import 'package:flutter/services.dart';
import 'package:hardware_keystore/hardware_keystore.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String _platformVersion = 'Unknown';
  String _apiResult = '';
  String? encodedMessage;
  String? signature;
  String? publicKey;
  String? encryptedMessage;
  String message = "VoRijk Database Encryption Key!";

  @override
  void initState() {
    super.initState();
    initPlatformState();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    String platformVersion;
    // Platform messages may fail, so we use a try/catch PlatformException.
    // We also handle the message potentially returning null.
    try {
      platformVersion = await HardwareKeystore.getPlatformVersion() ??
          'Unknown platform version';
    } on PlatformException {
      platformVersion = 'Failed to get platform version.';
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      _platformVersion = platformVersion;
    });
  }

  Future<void> setResultState(String? result) async {
    setState(() {
      _apiResult = result ?? 'Returned null';
    });
  }

  @override
  Widget build(BuildContext context) {
    String? result = '';

    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Hardware Keystore example app'),
        ),
        body: SafeArea(
          child: SingleChildScrollView(
            child: Column(
              children: [
                Text('Running on: $_platformVersion\n'),
                Text('KEYS', style: Theme.of(context).textTheme.bodyMedium),
                const Divider(),
                ElevatedButton(
                  onPressed: () async {
                    bool? success;
                    try {
                      await HardwareKeystore.deleteKeyPairs(
                        'keyId',
                      );
                      result = "deleted all keypairs";
                    } catch (error) {
                      result = error.toString();
                    }

                    await setResultState(success.toString());
                  },
                  child: const Text('Delete all key pairs'),
                ),
                const Divider(),
                ElevatedButton(
                  onPressed: () async {
                    try {
                      await HardwareKeystore.generateSigningKey('keyId');

                      result = "generation of signing keys successfully";
                    } catch (error) {
                      result = error.toString();
                    }

                    await setResultState(result.toString());
                  },
                  child: const Text('Generate signing keys'),
                ),
                ElevatedButton(
                  onPressed: () async {
                    bool? success;
                    try {
                      await HardwareKeystore.deleteSigningKey(
                        'keyId',
                      );
                      result = "signing keys deleted";
                    } catch (error) {
                      result = error.toString();
                    }

                    await setResultState(success.toString());
                  },
                  child: const Text('Delete signing key pair'),
                ),
                ElevatedButton(
                  onPressed: () async {
                    try {
                      result = await HardwareKeystore.getPublicKey(
                        'keyId',
                      );
                      publicKey = result!;
                    } catch (error) {
                      result = error.toString();
                    }

                    await setResultState(result);
                  },
                  child: const Text('Get public key'),
                ),
                const Divider(),
                Text('ECDSA', style: Theme.of(context).textTheme.bodyMedium),
                ElevatedButton(
                  onPressed: () async {
                    try {
                      result = await HardwareKeystore.sign('keyId', 'message');
                      setState(() {
                        signature = result!;
                      });
                    } catch (error) {
                      result = error.toString();
                    }

                    await setResultState(result);
                  },
                  child: const Text('Sign'),
                ),
                ElevatedButton(
                  onPressed: () async {
                    try {
                      final res = await HardwareKeystore.verifyMessage(
                        'keyId',
                        'message',
                        signature!,
                      );

                      result = res.toString();
                    } catch (error) {
                      result = "Verify failed: ${error.toString()}";
                    }

                    await setResultState(result.toString());
                  },
                  child: const Text('Verify'),
                ),
                const Divider(),
                Text('ECIES', style: Theme.of(context).textTheme.bodyMedium),
                ElevatedButton(
                  onPressed: () async {
                    try {
                      await HardwareKeystore.generateEncryptionKey('keyId');
                      result = "encryptionKeys generated";
                    } catch (error) {
                      result = "key pair generation failed";
                    }
                    await setResultState(result.toString());
                  },
                  child: const Text('generate encryption keys'),
                ),
                ElevatedButton(
                  onPressed: () async {
                    try {
                      await HardwareKeystore.deleteEncryptionKey('keyId');
                      result = "encryptionKeys delete";
                    } catch (error) {
                      result = "key pair deletion failed";
                    }
                    await setResultState(result.toString());
                  },
                  child: const Text('delete encryption keys'),
                ),
                ElevatedButton(
                  onPressed: () async {
                    try {
                      result = await HardwareKeystore.encrypt(
                        'keyId',
                        message,
                      );
                      setState(() {
                        encryptedMessage = result!;
                      });
                    } catch (error) {
                      result = error.toString();
                    }
                    await setResultState(result);
                  },
                  child: const Text('Encrypt'),
                ),
                ElevatedButton(
                  onPressed: encryptedMessage == null
                      ? null
                      : () async {
                          try {
                            result = await HardwareKeystore.decrypt(
                              'keyId',
                              encryptedMessage!,
                            );
                          } catch (error) {
                            result = error.toString();
                          }

                          await setResultState(result!);
                        },
                  child: const Text('Decrypt'),
                ),
                const Divider(),
                const Text(
                  'Result',
                  style: TextStyle(fontSize: 22),
                ),
                Text(_apiResult),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
