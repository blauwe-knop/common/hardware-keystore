// This is a basic Flutter integration test.
//
// Since integration tests run in a full Flutter application, they can interact
// with the host side of a plugin implementation, unlike Dart unit tests.
//
// For more information about Flutter integration tests, please see
// https://docs.flutter.dev/cookbook/testing/integration/introduction

import 'package:flutter_test/flutter_test.dart';
import 'package:integration_test/integration_test.dart';

import 'package:hardware_keystore/hardware_keystore.dart';

void main() {
  IntegrationTestWidgetsFlutterBinding.ensureInitialized();

  testWidgets('getPlatformVersion test', (WidgetTester tester) async {
    final String? version = await HardwareKeystore.getPlatformVersion();
    // The version string depends on the host platform running the test, so
    // just assert that some non-empty string is returned.
    expect(version, "14");
  });

  testWidgets("generateKeyPair test", (WidgetTester tester) async {
    const String keyId = "VORIJK_Key";
    final bool? _ = await HardwareKeystore.generateKeyPairs(keyId);
    final String? publicKey = await HardwareKeystore.getPublicKey(keyId);
    expect(publicKey?.isNotEmpty, true);
  });

  testWidgets("Sign message", (WidgetTester test) async {
    const String keyId = "VORIJK_Key";
    const String message = "test message";
    final String? signature = await HardwareKeystore.sign(keyId, message);

    final String? publicKey = await HardwareKeystore.getPublicKey(keyId);
    final bool? verification =
        await HardwareKeystore.verifyMessage(publicKey!, message, signature!);
    expect(verification, true);
  });
}
