import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:hardware_keystore/hardware_keystore_method_channel.dart';

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();

  MethodChannelHardwareKeystore platform = MethodChannelHardwareKeystore();
  const MethodChannel _ = MethodChannel('hardware_keystore');

  setUp(() {});

  tearDown(() {});

  test('getPlatformVersion', () async {
    expect(await platform.getPlatformVersion(), '42');
  });
}
