variables:
    FLUTTER_VERSION: 3.24.4

.flutter_docker_image:
  image: ghcr.io/cirruslabs/flutter:$FLUTTER_VERSION
  
stages:
  - lint
  - scan
  - test # note: the Jobs/Dependency-Scanning.gitlab-ci.yml template needs this  
  - release
  - tag 

dart_analyze:
  extends: .flutter_docker_image
  stage: lint
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
      changes:
        - VERSION
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
  script:
    - flutter analyze --pub
    - dart format . --set-exit-if-changed
  tags:
    - saas-linux-small-amd64

sast:
  stage: scan
  variables:
    SAST_EXCLUDED_ANALYZERS: spotbugs"
include:
  - template: Security/SAST.gitlab-ci.yml
  - template: Jobs/Dependency-Scanning.gitlab-ci.yml
  - template: Jobs/Container-Scanning.gitlab-ci.yml
    rules:
      - if: $CI_COMMIT_TAG

owasp_dependency_scan:
  image: docker:latest
  stage: scan
  services:
    - docker:dind
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
      changes:
        - VERSION
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script:
    - DC_VERSION="latest"
    - DC_DIRECTORY=$HOME/OWASP-Dependency-Check
    - 'DC_PROJECT="dependency-check scan: $(pwd)"'
    - DATA_DIRECTORY="$DC_DIRECTORY/data"
    - CACHE_DIRECTORY="$DC_DIRECTORY/data/cache"
    - mkdir -p "$DATA_DIRECTORY"
    - mkdir -p "$CACHE_DIRECTORY"
    - docker pull registry.gitlab.com/blauwe-knop/tools/owasp-dependency-scan:$DC_VERSION
    - |
      docker run --rm \
      -e user=$USER \
      -u $(id -u ${USER}):$(id -g ${USER}) \
      --volume $(pwd):/src:z \
      --volume $(pwd)/odc-reports:/report:z \
      registry.gitlab.com/blauwe-knop/tools/owasp-dependency-scan:$DC_VERSION \
      --scan /src \
      --format "ALL" \
      --project "$DC_PROJECT" \
      --out /report \
      --failOnCVSS 1 \
      --enableExperimental
  artifacts:
    paths:
      - odc-reports/dependency-check-report.html
      - odc-reports/dependency-check-report.json
      - odc-reports/dependency-check-gitlab.json
    expose_as: owasp-report
    when: always
    expire_in: 1 month
  allow_failure: true
  tags:
    - saas-linux-small-amd64

trivy_vulnerability_scan:
  image: docker:latest
  stage: scan
  services:
    - docker:dind
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
      changes:
        - VERSION
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
  script:
    - VERSION="0.53.0"
    - |
      docker run \
      -v /var/run/docker.sock:/var/run/docker.sock \
      -v $HOME/Library/Caches:/root/.cache/ \
      -v $PWD:/myapp \
      aquasec/trivy:$VERSION repo \
      --scanners vuln,misconfig,secret,license \
      --ignore-unfixed \
      --exit-code 1 \
      /myapp
  allow_failure:
    exit_codes:
      - 1
  tags:
    - saas-linux-small-amd64

increment_and_push_release:
  image: alpine:latest
  stage: release
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
      changes:
        - VERSION
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
  before_script:
    - apk add --update --no-cache git curl jq
    - git config --global user.email "${BK_CI_JOB_USER}@noreply.gitlab.com"
    - git config --global user.name "BK GitLab CI"
  script:
    - cat VERSION
    - 'export RELEASE_VERSION=$(cat VERSION)'
    - NEXT_RELEASE_VERSION=$(echo $RELEASE_VERSION | awk -F. -v OFS=. '{$NF += 1 ; print}')
    - echo $NEXT_RELEASE_VERSION > VERSION
    - cat VERSION
    - sed -i "s/^\(version:\).*$/\1 $NEXT_RELEASE_VERSION/g" pubspec.yaml
    - git add pubspec.yaml
    - git add VERSION
    - git commit -m "Release $NEXT_RELEASE_VERSION"
    - git push "https://${BK_CI_JOB_USER}:${BK_CI_JOB_TOKEN}@gitlab.com/blauwe-knop/common/hardware-keystore.git" HEAD:master

create_and_push_version_tag:
  image: alpine:latest
  stage: tag
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
      changes:
        -  VERSION
  before_script:
    - apk add --update --no-cache git
    - git config --global user.email "${BK_CI_JOB_USER}@noreply.gitlab.com"
    - git config --global user.name "BK GitLab CI"
  script:
    - cat VERSION
    - 'export RELEASE_VERSION=$(cat VERSION)'
    - git tag $RELEASE_VERSION
    - git push "https://${BK_CI_JOB_USER}:${BK_CI_JOB_TOKEN}@gitlab.com/blauwe-knop/common/hardware-keystore.git" tag $RELEASE_VERSION    