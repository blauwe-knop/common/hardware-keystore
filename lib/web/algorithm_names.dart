enum AlgorithmNames {
  aesCtr('AES/CTR'),
  aesGcm('AES/GCM'),
  sha256Hmac('SHA-256/HMAC'),
  sha256ConcatKdf('SHA-256/ConcatKDF'),
  sha256ecdsa('SHA-256/ECDSA'),
  ec('EC');

  const AlgorithmNames(this.value);
  final String value;
}
