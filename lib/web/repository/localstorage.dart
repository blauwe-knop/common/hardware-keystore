import 'dart:convert';
import 'dart:html';

import 'package:hardware_keystore/web/repository/model/aes_key.dart';
import 'package:hardware_keystore/web/repository/model/ec_key.dart';

class KeyRepository {
  final Storage _localStorage = window.localStorage;

  Future<void> storeEcKey(String keyId, EcKey key) async {
    _localStorage[keyId] = json.encode(key);
  }

  Future<EcKey> getEcKey(String keyId) async {
    if (!_localStorage.containsKey(keyId)) {
      throw Exception("key $keyId not found");
    }

    final keyJson = json.decode(_localStorage[keyId]!);

    final key = EcKey.fromJson(keyJson);

    return key;
  }

  Future<void> storeAesKey(String keyId, AesKey key) async {
    _localStorage[keyId] = json.encode(key);
  }

  Future<AesKey> getAesKey(String keyId) async {
    if (!_localStorage.containsKey(keyId)) {
      throw Exception("key $keyId not found");
    }

    final keyJson = json.decode(_localStorage[keyId]!);

    final key = AesKey.fromJson(keyJson);

    return key;
  }

  Future<void> delete(String keyId) async {
    _localStorage.remove(keyId);
  }
}
