class AesKey {
  String aesKeyBase64String;

  AesKey(this.aesKeyBase64String);

  Map<String, dynamic> toJson() {
    return {
      'aesKeyBase64String': aesKeyBase64String,
    };
  }

  factory AesKey.fromJson(Map<String, dynamic> json) {
    return AesKey(
      json['aesKeyBase64String'] as String,
    );
  }
}
