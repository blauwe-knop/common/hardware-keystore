class EcKey {
  String publicKeyPem;
  String privateKeyPem;

  EcKey(this.publicKeyPem, this.privateKeyPem);

  Map<String, dynamic> toJson() {
    return {
      'publicKeyPem': publicKeyPem,
      'privateKeyPem': privateKeyPem,
    };
  }

  factory EcKey.fromJson(Map<String, dynamic> json) {
    return EcKey(
      json['publicKeyPem'] as String,
      json['privateKeyPem'] as String,
    );
  }
}
