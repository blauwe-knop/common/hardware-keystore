import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:hardware_keystore/hardware_keystore_exception.dart';

import 'hardware_keystore_platform_interface.dart';

/// An implementation of [HardwareKeystorePlatform] that uses method channels.
class MethodChannelHardwareKeystore extends HardwareKeystorePlatform {
  /// The method channel used to interact with the native platform.
  @visibleForTesting
  final methodChannel = const MethodChannel('hardware_keystore');

  @override
  Future<String?> getPlatformVersion() async {
    try {
      return await methodChannel.invokeMethod<String>('getPlatformVersion');
    } on PlatformException catch (e, s) {
      throw HardwareKeystoreException("getPlatformVersion() failed", e, s);
    }
  }

  @override
  Future<String?> encrypt(String keyId, String payload) async {
    var arguments = {
      'identifier': keyId,
      'payload': payload,
    };

    try {
      return await methodChannel.invokeMethod<String>('encrypt', arguments);
    } on PlatformException catch (e, s) {
      throw HardwareKeystoreException("encrypt() failed", e, s);
    }
  }

  @override
  Future<String?> decrypt(String keyId, String cipher) async {
    var arguments = {
      'identifier': keyId,
      'cipher': cipher,
    };

    try {
      return await methodChannel.invokeMethod<String>('decrypt', arguments);
    } on PlatformException catch (e, s) {
      throw HardwareKeystoreException("decrypt() failed", e, s);
    }
  }

  @override
  Future<String?> sign(String keyId, String message) async {
    var arguments = {'identifier': keyId, 'message': message};

    try {
      return await methodChannel.invokeMethod<String>('sign', arguments);
    } on PlatformException catch (e, s) {
      throw HardwareKeystoreException("sign() failed", e, s);
    }
  }

  @override
  Future<String?> getPublicKey(String keyId) async {
    var arguments = {
      'identifier': keyId,
    };

    try {
      return await methodChannel.invokeMethod('getPublicKey', arguments);
    } on PlatformException catch (e, s) {
      throw HardwareKeystoreException("getPublicKey() failed", e, s);
    }
  }

  @override
  Future<bool?> deleteKeyPair(String keyId) async {
    var arguments = {
      'identifier': keyId,
    };

    try {
      return await methodChannel.invokeMethod('deleteKeyPairs', arguments);
    } on PlatformException catch (e, s) {
      throw HardwareKeystoreException("deleteKeyPair() failed", e, s);
    }
  }

  @override
  Future<bool?> verifyMessage(
      String keyId, String message, String signature) async {
    var arguments = {
      'identifier': keyId,
      'message': message,
      'signature': signature,
    };

    try {
      return await methodChannel.invokeMethod('verify', arguments);
    } on PlatformException catch (e, s) {
      throw HardwareKeystoreException("verify() failed", e, s);
    }
  }

  @override
  Future<bool?> generateKeyPairs(String keyId) async {
    var arguments = {'identifier': keyId};

    try {
      return await methodChannel.invokeMethod<bool?>(
          'generateKeyPairs', arguments);
    } on PlatformException catch (e, s) {
      throw HardwareKeystoreException("generateKeyPair() failed", e, s);
    }
  }

  @override
  Future<void> generateSigningKey(String keyId) async {
    var arguments = {'identifier': keyId};

    try {
      return await methodChannel.invokeMethod<void>(
          'generateSigningKey', arguments);
    } on PlatformException catch (e, s) {
      throw HardwareKeystoreException("generateSingingKey() failed", e, s);
    }
  }

  @override
  Future<void> deleteSigningKey(String keyId) async {
    var arguments = {'identifier': keyId};

    try {
      return await methodChannel.invokeMethod<void>(
          "deleteSigningKey", arguments);
    } on PlatformException catch (e, s) {
      throw HardwareKeystoreException("deleteSigningKey() failed", e, s);
    }
  }

  @override
  Future<void> generateEncryptionKey(String keyId) async {
    var arguments = {'identifier': keyId};

    try {
      return await methodChannel.invokeMethod<void>(
          "generateEncryptionKey", arguments);
    } on PlatformException catch (e, s) {
      throw HardwareKeystoreException("generateEncryptionKey() failed", e, s);
    }
  }

  @override
  Future<void> deleteEncryptionKey(String keyId) async {
    var arguments = {'identifier': keyId};

    try {
      return await methodChannel.invokeMethod<void>(
          "deleteEncryptionKey", arguments);
    } on PlatformException catch (e, s) {
      throw HardwareKeystoreException("deleteEncryptionKey() failed", e, s);
    }
  }
}
