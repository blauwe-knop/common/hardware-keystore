class HardwareKeystoreException implements Exception {
  final String message;
  final StackTrace stacktrace;
  Exception exception;

  HardwareKeystoreException(this.message, this.exception, this.stacktrace);
}
