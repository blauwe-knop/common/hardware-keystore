// In order to *not* need this ignore, consider extracting the "web" version
// of your plugin as a separate package, instead of inlining it in the same
// package as the core of your plugin.
// ignore: avoid_web_libraries_in_flutter

import 'dart:convert';
import 'dart:typed_data';

import 'package:basic_utils/basic_utils.dart';
import 'package:flutter_web_plugins/flutter_web_plugins.dart';
import 'package:hardware_keystore/web/repository/localstorage.dart';
import 'package:hardware_keystore/web/repository/model/aes_key.dart';
import 'package:hardware_keystore/web/repository/model/ec_key.dart';

import "package:pointycastle/export.dart";
import 'package:web/web.dart' as web;

import 'package:basic_utils/basic_utils.dart' as utils;

import 'package:pointycastle/key_generators/api.dart' as api;
import 'package:hardware_keystore/hardware_keystore_platform_interface.dart';
import 'package:hardware_keystore/web/curve.dart';
import 'package:hardware_keystore/web/secure_random.dart';
import 'package:hardware_keystore/web/algorithm_names.dart';
import 'package:pointycastle/asn1.dart';

/// A web implementation of the HardwareKeystorePlatform of the HardwareKeystore plugin.
class HardwareKeystoreWeb extends HardwareKeystorePlatform {
  final _keyRepository = KeyRepository();

  static const int macSize = 128; // 128 bits
  static const int aesKeySize = 16; // 16 bytes
  static const int nonceSize = 12; // 12 bytes
  static const String ecKeyIdPrefix = "ec_";
  static const String aesKeyIdPrefix = "aes_";

  /// Constructs a HardwareKeystoreWeb
  HardwareKeystoreWeb();

  static void registerWith(Registrar registrar) {
    HardwareKeystorePlatform.instance = HardwareKeystoreWeb();
  }

  /// Returns a [String] containing the version of the platform.
  @override
  Future<String?> getPlatformVersion() async {
    final version = web.window.navigator.userAgent;
    return version;
  }

  @override
  Future<String> decrypt(String keyId, String cipher) async {
    final cipherText = base64Decode(cipher);
    final nonce = Uint8List(nonceSize);
    final cipherTextWithTag = Uint8List(cipherText.length - nonce.length);
    nonce.setRange(0, nonce.length, cipherText);
    cipherTextWithTag.setRange(
        0, cipherTextWithTag.length, cipherText, nonce.length);

    final key = await _keyRepository.getAesKey(aesKeyIdPrefix + keyId);

    final aesKey = base64Decode(key.aesKeyBase64String);

    final blockCipher = GCMBlockCipher(AESEngine())
      ..init(
        false,
        AEADParameters(
          KeyParameter(aesKey),
          macSize,
          nonce,
          Uint8List(0),
        ),
      );

    final plaintext = blockCipher.process(cipherTextWithTag);
    final plainTextString = String.fromCharCodes(plaintext);
    return plainTextString;
  }

  @override
  Future<String> sign(String keyId, String message) async {
    final messageList = Uint8List.fromList(message.codeUnits);

    final key = await _keyRepository.getEcKey(ecKeyIdPrefix + keyId);

    final privateKey = CryptoUtils.ecPrivateKeyFromPem(key.privateKeyPem);

    final signer = utils.Signer(AlgorithmNames.sha256ecdsa.value)
      ..init(
          true,
          utils.ParametersWithRandom(
              utils.PrivateKeyParameter(privateKey), newSecureRandom()));
    final signature =
        signer.generateSignature(messageList) as utils.ECSignature;

    final signatureList = ASN1Sequence(
            elements: [ASN1Integer(signature.r), ASN1Integer(signature.s)])
        .encode();
    final signatureString = base64Encode(signatureList);

    return signatureString;
  }

  @override
  Future<String> encrypt(String keyId, String payload) async {
    final key = await _keyRepository.getAesKey(aesKeyIdPrefix + keyId);

    final aesKey = base64Decode(key.aesKeyBase64String);

    final cipher = GCMBlockCipher(AESEngine())
      ..init(
        true,
        AEADParameters(
          KeyParameter(aesKey),
          macSize,
          Uint8List(nonceSize),
          Uint8List(0),
        ),
      );

    final nonce = cipher.nonce;

    final ciphertext = cipher.process(Uint8List.fromList(payload.codeUnits));

    final output = Uint8List(nonce.length + ciphertext.length);
    output.setRange(0, nonce.length, nonce);
    output.setRange(nonce.length, output.length, ciphertext);
    final cryptText = base64Encode(output);
    return cryptText;
  }

// TODO: discuss should we always generate EC and AES keys together?
// TODO: discuss why do we return true? Could be a void function
  @override
  Future<bool> generateKeyPairs(String keyId) async {
    final ecParameters = ECDomainParameters(Curve.prime256v1.value);
    final keypair = (KeyGenerator(AlgorithmNames.ec.value)
          ..init(
            ParametersWithRandom(
              api.ECKeyGeneratorParameters(ecParameters),
              newSecureRandom(),
            ),
          ))
        .generateKeyPair();

    final publicKey = keypair.publicKey as ECPublicKey;
    final privateKey = keypair.privateKey as ECPrivateKey;

    final publicKeyPem = CryptoUtils.encodeEcPublicKeyToPem(publicKey);
    final privateKeyPem = CryptoUtils.encodeEcPrivateKeyToPem(privateKey);

    final aesKey = newSecureRandom().nextBytes(aesKeySize);

    final aesKeyBase64String = base64Encode(aesKey);

    await _keyRepository.storeEcKey(
        ecKeyIdPrefix + keyId, EcKey(publicKeyPem, privateKeyPem));

    await _keyRepository.storeAesKey(
        aesKeyIdPrefix + keyId, AesKey(aesKeyBase64String));

    return true;
  }

  @override
  Future<bool> verifyMessage(
      String keyId, String message, String signature) async {
    final signatureList = base64Decode(signature);
    final messageList = utf8.encode(message);

    // Parse signature
    utils.ECSignature ecdsaSig;
    try {
      final seq = ASN1Parser(signatureList).nextObject() as ASN1Sequence;
      if (seq.elements?.length != 2) {
        throw Exception('expected two ASN1 elements');
      }
      ecdsaSig = utils.ECSignature((seq.elements?[0] as ASN1Integer).integer!,
          (seq.elements?[1] as ASN1Integer).integer!);
    } catch (ex) {
      throw Exception('invalid signature: $ex');
    }

    final key = await _keyRepository.getEcKey(ecKeyIdPrefix + keyId);

    final ECPublicKey publicKey =
        CryptoUtils.ecPublicKeyFromPem(key.publicKeyPem);

    // Verify signature
    final verifier = utils.Signer(AlgorithmNames.sha256ecdsa.value)
      ..init(false, utils.PublicKeyParameter(publicKey));
    return verifier.verifySignature(messageList, ecdsaSig);
  }

  @override
  Future<bool> deleteKeyPair(String keyId) async {
    await _keyRepository.delete(keyId);

    return true;
  }

  @override
  Future<String> getPublicKey(String keyId) async {
    final key = await _keyRepository.getEcKey(ecKeyIdPrefix + keyId);

    return key.publicKeyPem;
  }

  @override
  Future<void> generateSigningKey(String keyId) async {
    final ecParameters = ECDomainParameters(Curve.prime256v1.value);
    final keypair = (KeyGenerator(AlgorithmNames.ec.value)
          ..init(
            ParametersWithRandom(
              api.ECKeyGeneratorParameters(ecParameters),
              newSecureRandom(),
            ),
          ))
        .generateKeyPair();

    final publicKey = keypair.publicKey as ECPublicKey;
    final privateKey = keypair.privateKey as ECPrivateKey;

    final publicKeyPem = CryptoUtils.encodeEcPublicKeyToPem(publicKey);
    final privateKeyPem = CryptoUtils.encodeEcPrivateKeyToPem(privateKey);
    await _keyRepository.storeEcKey(
        ecKeyIdPrefix + keyId, EcKey(publicKeyPem, privateKeyPem));
  }

  @override
  Future<void> deleteSigningKey(String keyId) async {
    await _keyRepository.delete(ecKeyIdPrefix + keyId);
  }

  @override
  Future<void> generateEncryptionKey(String keyId) async {
    final aesKey = newSecureRandom().nextBytes(aesKeySize);

    final aesKeyBase64String = base64Encode(aesKey);

    await _keyRepository.storeAesKey(
        aesKeyIdPrefix + keyId, AesKey(aesKeyBase64String));
  }

  @override
  Future<void> deleteEncryptionKey(String keyId) async {
    await _keyRepository.delete(aesKeyIdPrefix + keyId);
  }
}
