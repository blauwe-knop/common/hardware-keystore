import 'hardware_keystore_platform_interface.dart';

class HardwareKeystore {
  /// returns the platform version of the device the library is running on
  static Future<String?> getPlatformVersion() {
    return HardwareKeystorePlatform.instance.getPlatformVersion();
  }

  /// signs the given message with the private signing key in the keystore
  ///
  static Future<String?> sign(String keyId, String message) {
    return HardwareKeystorePlatform.instance.sign(keyId, message);
  }

  /// verifies the given signature for the message and keyId.
  /// Only works for messages signed with the sign method
  static Future<bool?> verifyMessage(
      String keyId, String message, String signature) {
    return HardwareKeystorePlatform.instance
        .verifyMessage(keyId, message, signature);
  }

  /// gets the public key of the signing key pair
  static Future<String?> getPublicKey(String keyId) {
    return HardwareKeystorePlatform.instance.getPublicKey(keyId);
  }

//TODO: discuss deleteKeyPairs vs deleteKey?
  /// deletes all keypairs with given keyId
  static Future<bool?> deleteKeyPairs(String keyId) {
    return HardwareKeystorePlatform.instance.deleteKeyPair(keyId);
  }

  /// generates a signing and encryption key pair for given keyId
  static Future<bool?> generateKeyPairs(String keyId) {
    return HardwareKeystorePlatform.instance.generateKeyPairs(keyId);
  }

  /// generates keys needed for signing
  static Future<void> generateSigningKey(String keyId) {
    return HardwareKeystorePlatform.instance.generateSigningKey(keyId);
  }

  /// delete the signing keys
  static Future<void> deleteSigningKey(String keyId) {
    return HardwareKeystorePlatform.instance.deleteSigningKey(keyId);
  }

  /// generate keys needed for encryption
  static Future<void> generateEncryptionKey(String keyId) {
    return HardwareKeystorePlatform.instance.generateEncryptionKey(keyId);
  }

  /// delete encryptionkeys
  static Future<void> deleteEncryptionKey(String keyId) {
    return HardwareKeystorePlatform.instance.deleteEncryptionKey(keyId);
  }

  /// encrypts the given plaintext with the encryption key for the given keyId
  static Future<String?> encrypt(String keyId, String plainText) {
    return HardwareKeystorePlatform.instance.encrypt(keyId, plainText);
  }

  /// decrypts the given ciphertext with with the encryption key for the given keyId
  static Future<String?> decrypt(String keyId, String cipherText) {
    return HardwareKeystorePlatform.instance.decrypt(keyId, cipherText);
  }
}
