import 'package:plugin_platform_interface/plugin_platform_interface.dart';

import 'hardware_keystore_method_channel.dart';

abstract class HardwareKeystorePlatform extends PlatformInterface {
  HardwareKeystorePlatform() : super(token: _token);

  static final Object _token = Object();

  static HardwareKeystorePlatform _instance = MethodChannelHardwareKeystore();

  /// The default instance of [HardwareKeystorePlatform] to use.
  ///
  /// Defaults to [MethodChannelHardwareKeystore].
  static HardwareKeystorePlatform get instance => _instance;

  /// Platform-specific implementations should set this with their own
  /// platform-specific class that extends [HardwareKeystorePlatform] when
  /// they register themselves.
  static set instance(HardwareKeystorePlatform instance) {
    PlatformInterface.verifyToken(instance, _token);
    _instance = instance;
  }

  Future<String?> getPlatformVersion() {
    throw UnimplementedError('platformVersion() has not been implemented.');
  }

  Future<String?> sign(String keyId, String message) {
    throw UnimplementedError("sign() has not been implemented.");
  }

  Future<String?> getPublicKey(String keyId) {
    throw UnimplementedError("getPublicKey() has not been implemented.");
  }

  Future<bool?> deleteKeyPair(String keyId) {
    throw UnimplementedError("deleteKeyPair() has not been implemented.");
  }

  Future<bool?> verifyMessage(String keyId, String message, String signature) {
    throw UnimplementedError("verify() has not been implemented.");
  }

  Future<bool?> generateKeyPairs(String keyId) {
    throw UnimplementedError("generateKeyPair() has not been implemented.");
  }

  Future<void> generateSigningKey(String keyId) {
    throw UnimplementedError("generateSigningKeys() has not been implemented.");
  }

  Future<void> deleteSigningKey(String keyId) {
    throw UnimplementedError("deleteSigningKeys() has not been implemented.");
  }

  Future<void> generateEncryptionKey(String keyId) {
    throw UnimplementedError(
        "generateEncryptionKeys() has not been implemented.");
  }

  Future<void> deleteEncryptionKey(String keyId) {
    throw UnimplementedError(
        "deleteEncryptionKeys() has not been implemented.");
  }

  Future<String?> encrypt(String keyId, String payload) {
    throw UnimplementedError("Encrypt() has not been implemented.");
  }

  Future<String?> decrypt(String keyId, String cipher) {
    throw UnimplementedError("Decrypt() has not been implemented.");
  }
}
