package nl.vorijk.hardware_keystore.keystore

import android.content.Context
import android.os.Build
import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyProperties
import androidx.annotation.RequiresApi
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.byteArrayPreferencesKey
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.intPreferencesKey
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.runBlocking
import nl.vorijk.hardware_keystore.util.DeviceUtils
import nl.vorijk.hardware_keystore.util.KeyStoreUtil
import nl.vorijk.hardware_keystore.util.setStrongBoxBackedCompat
import java.security.spec.AlgorithmParameterSpec
import javax.crypto.Cipher
import javax.crypto.SecretKey
import javax.crypto.spec.GCMParameterSpec



class EncryptionService(private val context: Context, private val datastore: DataStore<Preferences>){

    private val ivKey = byteArrayPreferencesKey("IV_KEY")
    private val tLenKey = intPreferencesKey("TLEN_KEY")
    private val keyStoreUtil = KeyStoreUtil()

    companion object {
        private const val ALGORITHM = KeyProperties.KEY_ALGORITHM_AES
        private const val KEY_SIZE = 256 // bytes
        private const val BLOCK_MODE = KeyProperties.BLOCK_MODE_GCM
        private const val PADDING = KeyProperties.ENCRYPTION_PADDING_NONE
        private const val TRANSFORMATION = "$ALGORITHM/$BLOCK_MODE/$PADDING"
        private const val ENCRYPT_KEY_PREFIX = "encryptionKey_"
    }


    @RequiresApi(Build.VERSION_CODES.S)
    fun createKey(identifier: String) {
        DeviceUtils.verifyDeviceUnlocked(context)
        val keyAlias = ENCRYPT_KEY_PREFIX + identifier
        try {
            keyStoreUtil.createKey(getAlgorithmSpec(keyAlias), ALGORITHM)
        } catch (ex: Exception) {
            throw KeyStoreKeyError.CreateKeyError(ex).keyException
        }
    }

    fun encrypt(identifier: String, payload: ByteArray): ByteArray {
        val key = getKey(identifier)
        val cipher = Cipher.getInstance(TRANSFORMATION)

        cipher.init(Cipher.ENCRYPT_MODE, key)
        val params = cipher.parameters.getParameterSpec(GCMParameterSpec::class.java)

        this.saveInitializationVector(params.iv)
        this.saveTlen(params.tLen)
        return cipher.doFinal(payload)
    }

    fun decrypt(identifier: String, payload: ByteArray): ByteArray {
        val key = getKey(identifier)
        val cipher = Cipher.getInstance(TRANSFORMATION)

        val iv = getInitializationVector()
        val tLen = getTlen()
        val gcmParams = GCMParameterSpec(tLen, iv)

        try {
            cipher.init(Cipher.DECRYPT_MODE, key, gcmParams)
        } catch (ex: Exception) {
            throw KeyStoreKeyError.DecryptCipherInitError(ex).keyException
        }

        try {
            return cipher.doFinal(payload)
        } catch (ex: Exception) {
            throw KeyStoreKeyError.DecryptionError(ex).keyException
        }
    }

    private fun getKey(identifier: String): SecretKey {
        DeviceUtils.verifyDeviceUnlocked(context)
        val keyAlias = ENCRYPT_KEY_PREFIX + identifier
        return keyStoreUtil.getSecretKey(keyAlias)
    }

    private fun getAlgorithmSpec(keyAlias: String): AlgorithmParameterSpec {
        return KeyGenParameterSpec.Builder(
            keyAlias,
            (KeyProperties.PURPOSE_ENCRYPT or KeyProperties.PURPOSE_DECRYPT)
        )
            .setBlockModes(BLOCK_MODE)
            .setEncryptionPaddings(PADDING)
            .setKeySize(KEY_SIZE)
            .setStrongBoxBackedCompat(context, true)
            .build()
    }

    private fun saveInitializationVector(initializationVector: ByteArray) {
        val preferencesDatastore = datastore
        runBlocking {
            preferencesDatastore.edit { prefs -> prefs[ivKey] = initializationVector }
        }
    }

    private fun saveTlen(tLen: Int) {
        val preferencesDatastore = datastore
        runBlocking {
            preferencesDatastore.edit { prefs -> prefs[tLenKey] = tLen }
        }
    }

    private fun getInitializationVector(): ByteArray {

        val preferencesDatastore = datastore
        return runBlocking {
            val ivFlow: Flow<ByteArray> = preferencesDatastore.data
                .map { preferences ->
                    preferences[ivKey] as ByteArray
                }
            ivFlow.first()
        }
    }

    private fun getTlen(): Int {
        val preferencesDatastore = datastore
        return runBlocking {
            val tLenFlow: Flow<Int> = preferencesDatastore.data
                .map { preferences ->
                    preferences[tLenKey] as Int
                }
            tLenFlow.first()
        }
    }

    fun deleteEntry(identifier: String) = keyStoreUtil.delete(ENCRYPT_KEY_PREFIX +identifier)

    fun clean() =
        keyStoreUtil.clean()
}