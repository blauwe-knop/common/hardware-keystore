package nl.vorijk.hardware_keystore.keystore

import android.content.Context
import android.os.Build
import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyProperties
import androidx.annotation.RequiresApi
import nl.vorijk.hardware_keystore.util.DeviceUtils
import nl.vorijk.hardware_keystore.util.KeyStoreUtil
import nl.vorijk.hardware_keystore.util.setStrongBoxBackedCompat
import java.security.KeyStoreException
import java.security.NoSuchAlgorithmException
import java.security.PrivateKey
import java.security.Signature
import java.security.UnrecoverableKeyException
import java.security.cert.Certificate
import java.security.spec.AlgorithmParameterSpec
import java.security.spec.ECGenParameterSpec
import java.util.Base64

const val SIGNATURE_ALGORITHM = "SHA256withECDSA"

class SigningService(val context: Context)  {
    private val keyStoreUtil = KeyStoreUtil()

    companion object {
        private const val SIGN_KEY_PREFIX = "signingKey_"
    }

    fun createKey(identifier: String) {
        DeviceUtils.verifyDeviceUnlocked(context)
        val keyAlias = SIGN_KEY_PREFIX + identifier
        try {
            keyStoreUtil.createKeyPair(getAlgorithmSpec(keyAlias), KeyProperties.KEY_ALGORITHM_EC)
            keyStoreUtil.enforceKeyInTrustedEnvironment(keyAlias)
        } catch (ex: Exception) {
            throw KeyStoreKeyError.CreateKeyError(ex).keyException
        }
    }

    @RequiresApi(Build.VERSION_CODES.S)
    fun publicKey(identifier: String): String {
        val key = getCertificate(identifier)
        val publicKey = key.publicKey
        try {
            val base64PubKey = Base64.getEncoder().encodeToString(publicKey.encoded)

            return "-----BEGIN PUBLIC KEY-----\n" +
                    base64PubKey.replace("(.{64})".toRegex(), "$1\n") +
                    "\n-----END PUBLIC KEY-----\n"
        } catch (ex: Exception) {
            throw KeyStoreKeyError.DeriveKeyError(ex).keyException
        }

    }

    fun sign(identifier: String, payload: ByteArray): ByteArray {
        val key = getPrivateKey(identifier)
        try {
            val signature = Signature.getInstance(SIGNATURE_ALGORITHM)
                .apply {
                    initSign(key)
                    update(payload)
                }
            return signature.sign()
        } catch (ex: Exception) {
            when (ex) {
                is UnrecoverableKeyException,
                is NoSuchAlgorithmException,
                is KeyStoreException -> throw KeyStoreKeyError.FetchKeyError(ex).keyException
            }
            throw KeyStoreKeyError.SignKeyError(ex).keyException
        }
    }

    fun verify(identifier: String, signature: ByteArray, message: ByteArray): Boolean {
        val key = getCertificate(identifier)
        val s = Signature.getInstance(SIGNATURE_ALGORITHM)
            .apply {
                initVerify(key.publicKey)
                update(message)
            }
        return s.verify(signature)
    }

    private fun getPrivateKey(identifier: String): PrivateKey {
        DeviceUtils.verifyDeviceUnlocked(context)
        val keyAlias = SIGN_KEY_PREFIX + identifier
        return keyStoreUtil.getPrivateKey(keyAlias) as PrivateKey
    }

    private fun getCertificate(identifier: String): Certificate {
        DeviceUtils.verifyDeviceUnlocked(context)
        val keyAlias = SIGN_KEY_PREFIX + identifier
        return keyStoreUtil.getCertificate(keyAlias)
    }

    private fun getAlgorithmSpec(keyAlias: String): AlgorithmParameterSpec {
        return KeyGenParameterSpec.Builder(keyAlias, KeyProperties.PURPOSE_SIGN)
            .setAlgorithmParameterSpec(ECGenParameterSpec("secp256r1"))
            .setDigests(KeyProperties.DIGEST_SHA256)
            .setStrongBoxBackedCompat(context, true)
            .build();
    }

    fun deleteEntry(identifier: String) = keyStoreUtil.delete(SIGN_KEY_PREFIX +identifier)

    fun clean() = keyStoreUtil.clean()
}