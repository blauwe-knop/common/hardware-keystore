package nl.vorijk.hardware_keystore.util

import android.app.KeyguardManager
import android.content.Context

fun Context.isDeviceLocked(): Boolean {
    val keyguardManager = this.getSystemService(Context.KEYGUARD_SERVICE) as? KeyguardManager
    return keyguardManager?.isKeyguardLocked == true
}
