package nl.vorijk.hardware_keystore.util

import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyInfo
import android.security.keystore.KeyProperties.SECURITY_LEVEL_STRONGBOX
import android.security.keystore.KeyProperties.SECURITY_LEVEL_TRUSTED_ENVIRONMENT
import androidx.annotation.RequiresApi
import nl.vorijk.hardware_keystore.BuildConfig
import nl.vorijk.hardware_keystore.util.DeviceUtils.isRunningOnEmulator
import java.security.Key
import java.security.KeyFactory
import java.security.KeyPairGenerator
import java.security.KeyStore
import java.security.NoSuchAlgorithmException
import java.security.NoSuchProviderException
import java.security.cert.Certificate
import java.security.spec.AlgorithmParameterSpec
import javax.crypto.KeyGenerator
import javax.crypto.SecretKey

class KeyStoreUtil {

    private val keyStore: KeyStore = KeyStore.getInstance(KEYSTORE_PROVIDER);

    companion object {
        private const val KEYSTORE_PROVIDER = "AndroidKeyStore"
    }

    init {
        keyStore.load(null)
    }

    @RequiresApi(Build.VERSION_CODES.S)
    @Throws(
        NoSuchProviderException::class, NoSuchAlgorithmException::class, IllegalStateException::class
    )
    fun createKey(algorithmKeySpec: AlgorithmParameterSpec, algorithm: String) {
        KeyGenerator.getInstance(algorithm, KEYSTORE_PROVIDER).apply {
            init(algorithmKeySpec)
            generateKey()
        }
    }

    fun createKeyPair(algorithmKeySpec: AlgorithmParameterSpec, algorithm: String) {
        KeyPairGenerator.getInstance(algorithm, KEYSTORE_PROVIDER).apply {
            initialize(algorithmKeySpec)
            generateKeyPair()
        }
    }

    fun getSecretKey(keyAlias: String): SecretKey {
        return (keyStore.getEntry(keyAlias, null) as KeyStore.SecretKeyEntry).secretKey
    }

    fun getPrivateKey(keyAlias: String): Key {
        return keyStore.getKey(keyAlias, null)
    }

    fun getCertificate(keyAlias: String): Certificate {
        val certificate = keyStore.getCertificate(keyAlias)

        return certificate
    }

    fun clean() {
        val aliases = keyStore.aliases()
        aliases.asSequence()
            .forEach(::delete)
    }

    private fun isHardwareBacked(keyAlias: String): Boolean {
        val securityLevel =  getSecurityLevelCompat(keyAlias)
        if (securityLevel == SECURITY_LEVEL_STRONGBOX) return true
        if (securityLevel == SECURITY_LEVEL_TRUSTED_ENVIRONMENT) return true
        @Suppress("DEPRECATION")
        return getKeyInfo(keyAlias).isInsideSecureHardware
    }

    private fun getKeyInfo(keyAlias: String): KeyInfo {
        val secretKey = keyStore.getKey(keyAlias, null)
        val keyFactory: KeyFactory =
            KeyFactory.getInstance(secretKey.algorithm, KEYSTORE_PROVIDER)
        return keyFactory.getKeySpec(secretKey, KeyInfo::class.java)
    }

    /**
     * Returns the securityLevel of this key, falls back to providing
     * null on devices with API level < 31
     */
    private fun getSecurityLevelCompat(keyAlias: String): Int? {
        val keyInfo = getKeyInfo(keyAlias)
        return runCatching<Int> {
            return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                keyInfo.securityLevel
            } else {
                null
            }
        }.getOrNull()
    }

    fun delete(keyAlias: String) {
        keyStore.deleteEntry(keyAlias)
    }

    /**
     * Enforce a key to be stored in the trusted execution environment.
     * if the app is running on an Emulator no enforcing takes place.
     * Else the key will be DELETED if the security level is below SECURITY_LEVEL_TRUSTED_ENVIRONMENT
     */
    fun enforceKeyInTrustedEnvironment(keyAlias: String) : Boolean {
        val allowSoftwareBackedKeys = isRunningOnEmulator && BuildConfig.DEBUG
        if (allowSoftwareBackedKeys) {
            return true
        }

        val keyStore: KeyStore = KeyStore.getInstance(KEYSTORE_PROVIDER)
        keyStore.load(null)
        if (!isHardwareBacked(keyAlias)) {
            keyStore.deleteEntry(keyAlias)
            return false
        }
        return true
    }
}

/**
 * Enable strongbox when this feature is available, otherwise
 * this call is simply ignored.
 */
fun KeyGenParameterSpec.Builder.setStrongBoxBackedCompat(
    context: Context,
    enable: Boolean
): KeyGenParameterSpec.Builder {
    val pm = context.packageManager
    val allowSoftwareBackedKeys = isRunningOnEmulator && BuildConfig.DEBUG
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P && pm.hasSystemFeature(PackageManager.FEATURE_STRONGBOX_KEYSTORE) && !allowSoftwareBackedKeys) {
        this.setIsStrongBoxBacked(enable)
    }
    return this
}
