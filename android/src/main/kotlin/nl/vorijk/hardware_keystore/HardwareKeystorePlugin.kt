package nl.vorijk.hardware_keystore

import android.app.Activity
import android.content.Context
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.datastore.preferences.core.byteArrayPreferencesKey
import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.embedding.engine.plugins.activity.ActivityAware
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result
import org.bouncycastle.util.encoders.Base64

enum class ExposedMethods(val methodName: String) {
    GETPLATFORMVERSION("getPlatformVersion"), GETPUBLICKEY("getPublicKey"), DELETEKEYPAIRS("deleteKeyPairs"),
    GENERATEKEYPAIRS("generateKeyPairs"), GENERATESIGNINGKEY("generateSigningKey"), DELETESIGNINGKEY(
        "deleteSigningKey"
    ),
    GENERATEENCRYPTIONKEY("generateEncryptionKey"), DELETEENCRYPTIONKEY("deleteEncryptionKey"),
    SIGN("sign"), VERIFY("verify"), ENCRYPT("encrypt"), DECRYPT("decrypt"),
}

/** HardwareKeystorePlugin */
class HardwareKeystorePlugin : FlutterPlugin, MethodCallHandler, ActivityAware {
    // The MethodChannel that will the communication between Flutter and native Android
    //
    // This local reference serves to register the plugin with the Flutter Engine and unregister it
    // when the Flutter Engine is detached from the Activity
    private lateinit var channel: MethodChannel
    private lateinit var context: Context
    private lateinit var activity: Activity
    private lateinit var platformSupport: PlatformSupport


    override fun onAttachedToEngine(flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
        channel = MethodChannel(flutterPluginBinding.binaryMessenger, "hardware_keystore")
        channel.setMethodCallHandler(this)
        context = flutterPluginBinding.applicationContext
        platformSupport = PlatformSupport.getInstance(context);
    }

    @RequiresApi(Build.VERSION_CODES.S)
    override fun onMethodCall(call: MethodCall, result: Result) {
        try {
            when (call.method) {
                ExposedMethods.GETPLATFORMVERSION.methodName -> result.success("Android ${Build.VERSION.RELEASE}")
                ExposedMethods.SIGN.methodName -> sign(call, result)
                ExposedMethods.GETPUBLICKEY.methodName -> getPublicKey(call, result)
                ExposedMethods.GENERATEKEYPAIRS.methodName -> generateKeyPairs(call, result)
                ExposedMethods.DELETEKEYPAIRS.methodName -> deleteKeyPair(result)
                ExposedMethods.VERIFY.methodName -> verify(call, result)
                ExposedMethods.ENCRYPT.methodName -> encrypt(call, result)
                ExposedMethods.DECRYPT.methodName -> decrypt(call, result)
                ExposedMethods.GENERATESIGNINGKEY.methodName -> generateSigningKey(call, result)
                ExposedMethods.DELETESIGNINGKEY.methodName -> deleteSigningKey(call, result)
                ExposedMethods.GENERATEENCRYPTIONKEY.methodName -> generateEncryptionKey(
                    call,
                    result
                )
                ExposedMethods.DELETEENCRYPTIONKEY.methodName -> deleteEncryptionKey(call, result)
                else -> result.notImplemented()
            }
        } catch (ex: Exception) {
            result.error(ex.localizedMessage!!, null, null)
        }
    }

    private fun sign(call: MethodCall, result: Result) {
        val id = call.argument<String>("identifier")!!
        val message = call.argument<String>("message")!!

        val signature = platformSupport.signingService.sign(
            id, message.toByteArray()
        )
        val base64String = Base64.toBase64String(signature)
        result.success(base64String)

    }

    @RequiresApi(Build.VERSION_CODES.S)
    private fun getPublicKey(call: MethodCall, result: Result) {
        val id = call.argument<String>("identifier")!!

        result.success(platformSupport.signingService.publicKey(id))
    }

    private fun verify(call: MethodCall, result: Result) {
        val id = call.argument<String>("identifier")!!
        val base64Signature = call.argument<String>("signature")!!
        val message = call.argument<String>("message")!!

        val signature = Base64.decode(base64Signature)
        val signatureValid =
            platformSupport.signingService.verify(id, signature, message.toByteArray())
        if (signatureValid) {
            result.success(true)
        } else {
            result.error("1", "Signature not valid", null)
        }
    }

    @RequiresApi(Build.VERSION_CODES.S)
    fun generateKeyPairs(call: MethodCall, result: Result) {
        val id = call.argument<String>("identifier")!!

        platformSupport.signingService.createKey(id)
        platformSupport.encryptionService.createKey(id)
        result.success(true)
    }


    private fun generateSigningKey(call: MethodCall, result: Result) {
        val id = call.argument<String>("identifier")!!

        platformSupport.signingService.createKey(id)
        result.success(true)
    }

    private fun deleteSigningKey(call: MethodCall, result: Result) {
        val id = call.argument<String>("identifier")!!

        platformSupport.signingService.deleteEntry(id)
        result.success(true)
    }

    @RequiresApi(Build.VERSION_CODES.S)
    private fun generateEncryptionKey(call: MethodCall, result: Result) {
        val id = call.argument<String>("identifier")!!

        platformSupport.encryptionService.createKey(id)
        result.success(true)
    }

    private fun deleteEncryptionKey(call: MethodCall, result: Result) {
        val id = call.argument<String>("identifier")!!

        platformSupport.encryptionService.deleteEntry(id)
        result.success(true)
    }

    private fun deleteKeyPair(result: Result) {
        platformSupport.signingService.clean()
        platformSupport.encryptionService.clean()
        result.success(true)
    }

    private fun encrypt(call: MethodCall, result: Result) {
        val id = call.argument<String>("identifier")!!
        val payload = call.argument<String>("payload")!!

        val cipher = platformSupport.encryptionService.encrypt(id, payload.toByteArray())
        val base64Cipher = Base64.toBase64String(cipher)
        result.success(base64Cipher)
    }

    private fun decrypt(call: MethodCall, result: Result) {
        val id = call.argument<String>("identifier")!!
        val base64cipher = call.argument<String>("cipher")!!

        val cipher = Base64.decode(base64cipher)
        val plainTextList = platformSupport.encryptionService.decrypt(id, cipher)
        result.success(plainTextList.toString(Charsets.UTF_8))
    }

    override fun onDetachedFromEngine(binding: FlutterPlugin.FlutterPluginBinding) {
        channel.setMethodCallHandler(null)
    }

    override fun onAttachedToActivity(binding: ActivityPluginBinding) {
        activity = binding.activity;
    }

    override fun onDetachedFromActivityForConfigChanges() {
        //Implementation not needed
    }

    override fun onReattachedToActivityForConfigChanges(binding: ActivityPluginBinding) {
        //Implementation not needed
    }

    override fun onDetachedFromActivity() {
        //Implementation not needed
    }


}
