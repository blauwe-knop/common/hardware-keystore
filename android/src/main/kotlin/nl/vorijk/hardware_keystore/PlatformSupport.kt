package nl.vorijk.hardware_keystore

import android.content.Context
import androidx.annotation.VisibleForTesting
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.preferencesDataStore
import nl.vorijk.hardware_keystore.keystore.EncryptionService
import nl.vorijk.hardware_keystore.keystore.SigningService

class PlatformSupport private constructor(context: Context) {

    val Context.dataStore: DataStore<Preferences> by preferencesDataStore(name = "settings")

    val signingService = SigningService(context)

    val encryptionService = EncryptionService(context, context.dataStore)

    companion object {
        @Volatile
        private var INSTANCE: PlatformSupport? = null

        fun getInstance(context: Context): PlatformSupport =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: PlatformSupport(context.applicationContext).also { INSTANCE = it }
            }
    }

}
